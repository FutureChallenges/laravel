<?php

use Local\Exception\HttpException;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


Route::filter('check_timestamps', function() {

	if (Auth::check()) {
		$user = Auth::user();
		// check if the user logged in before the last password change
		// if yes, we log him out and ask him to log in again

		if (Auth::getPasswordToken() != $user->password_token) {
			Auth::logout();
			return Redirect::guest("/login");
		}

		// update last_seen timestamp for the user		
		$user->timestamps = false;
		$user->last_seen = time();
		$user->save();
		$user->timestamps = true;
	}
});

Route::filter('check_tor', function($route, $request, $redirect) {


	if (Session::get('tor.error')) {
		// check if we should repeat the failed tor check 
		// we repeat the check with probability 40%
		$lottery = Config::get('tor.lottery');
		if (mt_rand(1, $lottery[1]) >= $lottery[0]) {
			Log::info("tor check for url " . $request->fullUrl() . " skipped");			
			return;
		}
	}

	$istor = Tor::check();

	Session::put('tor.warning', ($istor < 1));
	Session::put('tor.error', ($istor < 0));

    if (
    !Session::get('tor.optout') && 
    	$istor < 1) {
    	if ($redirect) {
			return Redirect::guest('torwarning');
		} else {
			throw new HttpException(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR, Lang::get('error.tor_not_in_use'));
		}
	}
});

Route::filter('form_traps', function() {

	if (Input::get('email2') || 
		Input::get('telephone') || 
		Input::get('name') ||
		Input::get('address')) {
		Log::info("FORM TRAPS TRIGGERED");
		return Redirect::route('home');
	}
});

Route::filter('remove_intended_url', function() {

	Session::forget('url.intended');
});

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('/login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException("csrf token mismatch");
	}
});