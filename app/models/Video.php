<?php

use Carbon\Carbon;

class Video extends Eloquent {

	static $categories = array(
		NULL,
		"ArmedConflict",
		"ArmsControl",
		"ChildrensRights",
		"DetentionAndImprisonment",
		"Discrimination",
		"EconomicSocialAndCulturalRights",
		"EnforcedDisappearances",
		"FreedomOfExpression",
		"HumanRightsDefenders",
		"HumanRightsEducation",
		"Impunity",
		"IndigenousPeoples",
		"Poverty",
		"RefugeesMigrantsAndInternallyDisplacedPersons",
		"Religion",
		"Torture",
		"Other"
	);


	const TIME_FORMAT = "d/m/Y";

	protected $guarded = array('id', 'created_at', 'updated_at');


	public function getDates() {

		return array_merge(parent::getDates(), array("time"));
	}

	public function getTimeAttribute($value) {

		return (new Carbon($value))->format(static::TIME_FORMAT);
	}


}