<?php namespace Local\Tor;

use \Exception;

class BadIPv4AddressException extends Exception {
	
	protected $address = null;

	public function getAddress() {
		return $this->address;
	}

	public function __construct($address) {

		parent::__construct($address . " is not a valid IPv4 address");

		$this->address = $address;
	}
}