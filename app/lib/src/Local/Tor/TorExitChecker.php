<?php namespace Local\Tor;

require_once "Net/DNS2.php";

use \Log;
use \Session;
use \Net_DNS2_Resolver;
use \Net_DNS2_Exception;
use \Net_DNS2_Lookups;

class TorExitChecker {

	protected $resolver;

	public function __construct(Net_DNS2_Resolver $resolver = null) {

		if (!$resolver) {
			$resolver = new Net_DNS2_Resolver();
		}
		$this->resolver = $resolver;
	}

	public function check($check_cache = true) {

		// get client request parameters from Apache or equiv server:
		$ip = $myip = $myport = 0;
		if (isset ($_SERVER["REMOTE_ADDR"])) { $ip = $_SERVER["REMOTE_ADDR"]; }
		if (isset ($_SERVER["SERVER_ADDR"])) { $myip = $_SERVER["SERVER_ADDR"]; }
		if (isset ($_SERVER["SERVER_PORT"])) { $myport = $_SERVER["SERVER_PORT"]; }
		$qh = static::torel_qh($ip, $myport, $myip);

		if ($check_cache) {
			$cache = Session::get("tor.check", array());
			if (array_key_exists($qh, $cache)) {
				return $cache[$qh];
			}
		}
		$answer = $this->torel_check($ip, $myport, $myip);
		if ($answer > -1) {
			Session::set("tor.check", array($qh => $answer));
		}
		return $answer;
	}

	public static function revaddr ($ip) {
  		$r = explode(".", $ip);
  		if (count($r) == 4) {
  			list($a, $b, $c, $d) = $r;
			return("${d}.${c}.${b}.${a}");
  		}
  		// not an IPv4 address
  		throw new BadIPv4AddressException($ip);
	}

	public static function torel_qh ($ip, $port, $destip) {

		$rsrcip = static::revaddr ($ip);
		$rdstip = static::revaddr ($destip);
		return("${rsrcip}.${port}.${rdstip}.ip-port.exitlist.torproject.org");
	}

	public function torel_check ($ip, $port, $destip) {
		$ndr = $this->resolver; 
		$qh = static::torel_qh($ip, $port, $destip);

		Log::info("sending tordnsel request for: " . $qh);

		// uncomment these two lines to query the server directly...
		//$ns = "exitlist-ns.torproject.org";
		//$ndr->nameservers( array($ns) );

		// tune DNS params accordingly.  this is just my preference.
		$ndr->retrans = 2;
		$ndr->retry = 3;
		$ndr->usevc = 0;

		// perform DNS query
		try {
			$pkt = $ndr->query($qh);			

			if (! isset($pkt->answer[0])) {
				// response but no answer section.  does not appear to be Tor exit.
				// (this should only happen when authority sections are provided without answer)
				return 0;
			}
			// is Tor exit
			return 1;		

		} catch (Net_DNS2_Exception $e) {

			if ($e->getCode() == Net_DNS2_Lookups::RCODE_NXDOMAIN) {
				 // response but no answer.  does not appear to be Tor exit.
				return 0;
			}
			// search failed: no response or other problem...
			return -1;
		}
	}
}
