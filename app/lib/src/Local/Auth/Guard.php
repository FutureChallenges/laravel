<?php namespace Local\Auth;

use \Illuminate\Auth\UserInterface;

class Guard extends \Illuminate\Auth\Guard {


	protected $user_id_path = "user_id";

	protected $password_token_path = "password_token";

	/**
	 * when this user logged in
	 * @var string (unix timestamp)
	 */
	protected $password_token = null;

	/**
	* tries to retrieve the user login information from either the session or the recaller cookie
	* updates $this->user and $this->login_ts. Returns true if user was found
	*
	* @return bool
	*/
	public function retrieveUser() {

		$data = $this->session->get($this->getName());
		$token = null;

		if (!$data) {
			$recaller = $this->getRecaller();
			if ($this->validRecaller($recaller)) {
				list($data, $token) = explode('|', $recaller, 2);
				$data = unserialize($data);
			}
		}

		if (!is_array($data))
			return false;

		$user_id = array_get($data, $this->user_id_path);

		if ($token) {
			$user = $this->provider->retrieveByToken($user_id, $token);				
		} else {
			$user = $this->provider->retrieveByID($user_id);
		}

		$this->password_token = array_get($data, $this->password_token_path);

		return !is_null($this->user = $user);
	}

	/**
	* Passowrd token (updated whenever the user changes the password)
	*
	* @return string
	*/
	public function getPasswordToken() {
		return $this->password_token;
	}

	/**
	 * Get the currently authenticated user.
	 *
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function user()
	{
		if ($this->loggedOut) return;

		if (is_null($this->user))
		{
			$this->retrieveUser();
		}

		return $this->user;
	}


	/**
	 * Log a user into the application.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  bool  $remember
	 * @return void
	 */
	public function login(UserInterface $user, $remember = false)
	{

		if (!$user->password_token) {
			$user->password_token = str_random(60);
			$user->save();
		}

		// If the user should be permanently "remembered" by the application we will
		// queue a permanent cookie that contains the encrypted user
		// identifier and login time. We will then decrypt this later to retrieve the users.
		// Otherwise, we store the user id and login time in the session
		if ($remember) {
			$this->createRememberTokenIfDoesntExist($user);

			$this->queueRecallerCookie($user);
		} else {
			$this->updateSession($user);
		}

		// If we have an event dispatcher instance set we will fire an event so that
		// any listeners will hook into the authentication events and run actions
		// based on the login and logout events fired from the guard instances.
		if (isset($this->events))
		{
			$this->events->fire('auth.login', array($user, $remember));
		}

		$this->setUser($user);
	}

	/**
	 * Log the given user ID into the application.
	 *
	 * @param  mixed  $id
	 * @param  bool   $remember
	 * @return \Illuminate\Auth\UserInterface
	 */
	public function loginUsingId($id, $remember = false)
	{
		$this->login($user = $this->provider->retrieveById($id), $remember);

		return $user;
	}

	/**
	* Create an array with the login data
	*
	* @return array
	*/
	public function makeLoginData($user) {

		return array(
				$this->user_id_path => $user->id,
				$this->password_token_path => $user->password_token
		);				
	}

	/**
	 * Update the session with the given ID.
	 *
	 * @param  string  $id
	 * @return void
	 */
	protected function updateSession( $user)
	{
		$this->session->put($this->getName(), $this->makeLoginData($user));

		$this->session->migrate(true);
	}

	/**
	 * Queue the recaller cookie into the cookie jar.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @return void
	 */
	protected function queueRecallerCookie(UserInterface $user)
	{
		$value = serialize($this->makeLoginData($user)).'|'.$user->getRememberToken();

		$this->getCookieJar()->queue($this->createRecaller($value));
	}

}