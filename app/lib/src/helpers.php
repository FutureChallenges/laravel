<?php

if (!function_exists('return_bytes')) {

	function return_bytes($val) {
	    $val = trim($val);
	    $last = strtolower($val[strlen($val)-1]);
	    switch($last) {
	        // The 'G' modifier is available since PHP 5.1.0
	        case 'g':
	            $val *= 1024;
	        case 'm':
	            $val *= 1024;
	        case 'k':
	            $val *= 1024;
	    }

	    return $val;
	}

}

if (!function_exists('ajaxform_json_encode')) {

	function ajaxform_json_encode($data) {

		$body = json_encode($data);

		if (Request::input('ajaxForm')) {			
			// for ajaxForms, we send it the serialized json as the content of a textarea HTML tag
			// this is because, in older browsers, ajaxForm places the response in an iframe that doesn't handle plain json content very well
			$body = '<textarea>' . $body . '</textarea>';			
		}

		return $body;
	}
}