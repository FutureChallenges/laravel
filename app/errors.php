<?php

use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

// Error handlers

App::error(function(TokenMismatchException $exception) {

	Log::error($exception);

	$error_code = Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR;

	if (Request::ajax() || Request::input('ajaxForm')) {

		return Response::make(ajaxform_json_encode(
					array('errorMsg' => $exception->getMessage())
					), $error_code);

	} else {
 
		return Redirect::route('home');

	}
});

if (!Config::get('app.debug')) {

	App::error(function(Exception $exception) {

		Log::error($exception);

		$error_code = SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR;
		return View::make('error')->with(array(
				'statusCode' => $error_code,
			 	'statusText' => SymfonyResponse::$statusTexts[$error_code],
			 	'errorMsg' => "Something went wrong"
		 	)
		);

	});
}

App::error(function(HttpException $exception) {

	Log::error($exception);

	if (Request::ajax() || Request::input('ajaxForm')) {

		// pack all the information about the exception in a json structure
		$json_data = array(
				"statusCode" => $exception->getStatusCode(),
				"statusText" => SymfonyResponse::$statusTexts[$exception->getStatusCode()],
				"errorMsg" => $exception->getMessage()			
			);

		return Response::make(ajaxform_json_encode($json_data), $exception->getStatusCode());			

	} else {

		return Response::view('error', array(
				'statusCode' => $exception->getStatusCode(),
			 	'errorMsg' => $exception->getMessage(),
			 	'statusText' => SymfonyResponse::$statusTexts[$exception->getStatusCode()]
		 	), $exception->getStatusCode());
	}	
});

