<?php

return array(
	"anonymity_header" => "WARNING: Your anonimity is compromised!",
	"anonymity_msg" => "It appears that you are not browsing this website through Tor!",
	"anonymity_ok_header" => "Good! It looks like you are using Tor",
	"anonymity_ok_msg" => "You are browsing this website anonymously. You should be good.",
	"anonymity_unclear_header" => "We could not determine if you are using Tor",
	"anonymity_unclear_msg" => "Please check if you are using the Tor browser.",

	"notor_header" =>  "WARNING!!! It appears that you are not using Tor",
	"notor_msg" => "In order to preserve your anonimity, you need to download and use the Tor Browser",
	"notor_unsure_header" => "Your Tor status could not be determined",
	"download_tor" => "Download Tor",
	"browse_without_tor" => "Browse without Tor",
	"js_disabled" => "Warning: JavaScript is not enabled in your browser",
	"js_message" => "Maybe you disabled JavaScript through your browser preferences or some extension (such as NoScript) is blocking it. You need to enable JavaScript to access the content of this page",
	"tor_optout_msg" => "You decided to opt out of using Tor while browsing our website. You won't be anonymous, thus potentially exposing yourself to risks."
);