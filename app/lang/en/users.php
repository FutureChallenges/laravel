<?php

return array(

	'login_instructions' => 'Here you can login using your account information.',
	'login_instructions_redirect' => 'Before proceeding to the requested page, you need to login using your account information.',
	'signup_instructions' => 'If you don\'t have an ' . trans('general.title') . ' account yet, you can create one here. You can sign up fully anonymously if you wish, or you can provide an e-mail address.',
	'profile_instructions' => 'Here you can change your password or edit your email address.',
	'login_failed' => 'Username and password are incorrect. Please try again.',
	'login_button' => 'Log in',
	'username' => 'Username:',
	'username_placeholder' => 'Choose a username here',
	'password' => 'Password:',
	'password_help' => 'Password has to be at least 6-charachters long.',
	'password_confirmation' => 'Confirm password:',
	'email' => 'E-Mail:',
	'email_help' => 'Your e-mail is not required but it will be useful if you forget your password. You can always add an e-mail to your profile later.',
	'email_placeholder' => 'Enter your e-mail here if you want to',
	'signup_button' => 'Sign up!',
	'error_warning' => 'Something went wrong. Please take a look at the error messages below.',
	'old_password' => 'Your password:',	
	'new_password' => 'New password:',
	'new_password_confirmation' => 'Confirm new password:',
	'update_button' => 'Update my profile',
	'old_password_help' => 'In order to make changes to your profile, you have to enter your current password',
	'password_change_instructions' => 'Fill in the fields below if you want to change your password.',
	'password_forgotten' => 'I forgot my password',
	'password_forgotten_question' => 'Did you forget your password?',
	'remember_me' => 'Keep me logged in',
	'remember_me_explained' => 'When this option is set, you will be logged out only when you explicitly click the "Log out" button',
	'signup_success' => 'Congratulations! You successfully signed up',
	'logout_success' => 'You successfully logged out',
	 'password_reset_msg' => 'If you provided us with your email address, you can reset your password here. We will send you an email with a new 
      	temporary password',
      'reset_password' => 'Reset password',
      'password_was_reset' => 'Your password was reset',
      'profile_update_success' => 'Your profile was successfully updated'

);