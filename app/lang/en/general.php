<?php


return array(
	
	"title" => "Irrepressible Voices",
	"welcome" => "Hello!",
	"welcome_prefix" => "Hello",
	"warning" => "Oops!",
	"learn_more" => "Learn more",
	"next_page_suggestion" => "You may now go to the",
	"click_to_continue" => "Click here to continue",
	"home_page" => "home page",
	"login_page" => "login page",
	"close" => "Close",
	"cancel" => "Cancel",
	"nothanks" => "No thanks",
	"copyright_note" => "&copy; 2014 Irrepressible Voices"

);