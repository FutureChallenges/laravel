<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaggingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tagging', function(Blueprint $table)
		{
			$table->timestamps();
			$table->integer('tag')->unsigned()->references('id')->on('tags');
			$table->integer('video')->unsigned()->references('id')->on('videos');
			$table->integer('user')->unsigned()->references('id')->on('users');
			$table->primary(array('tag', 'video'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tagging');
	}

}
