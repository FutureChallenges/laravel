<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaiseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('raise', function(Blueprint $table)
		{
			$table->timestamps();
			$table->integer('user')->unsigned()->references('id')->on('users');
			$table->integer('video')->unsigned()->references('id')->on('videos');
			$table->primary(array('user', 'video'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raise');
	}

}
