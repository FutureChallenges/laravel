<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('uploader')->unsigned()->references('id')->on('users');
			$table->string('title', 255);
			$table->string('location', 255);
			$table->string('category', 255);
			$table->datetime('time');
			$table->text('description');
			$table->text('statement');
			$table->integer('duration')->unsigned();
			$table->string('original_file', 255);
			$table->string('encoded_file', 255);
			$table->string('thumbnail_file', 255);
			$table->integer('positive_votes')->unsigned();
			$table->integer('negative_votes')->unsigned();
			$table->tinyInteger('status');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
