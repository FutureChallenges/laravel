<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// do this for all requests 
Route::when('*', array('before' => 'check_timestamps'));

// do this for all requests expect the Tor optout and warning and ajax
Route::whenRegex('/^(?!doOptout$)(?!torwarning$)(?!ajax\/)/', array('before' => 'check_tor:1'));

// for ajax call do a Tor check but without a redirect
Route::whenRegex('/^(?=ajax\/)/', array('before' => 'check_tor:0'));

// Cross-site request forgery protection and form traps for all post requests
Route::when('*', array('before' => 'csrf|form_traps'), array('post'));

// Tor opt out and warning
Route::post('doOptout', array('as' => 'doOptout', 'uses' => 'HomeController@optOut'));
Route::get('torwarning', array('as' => 'torwarning',  'uses' => 'HomeController@torwarning'));

// These routes make sense both for logged in and guest users so we remove the intended url
Route::group(array('after' => 'remove_intended_url'), function() {

	Route::get('', array('as' => 'home', 'uses' => 'HomeController@showWelcome'));	

	Route::get('toroptout_success', array('as' => 'toroptout_success', function() {  return View::make('toroptout'); }));
});


// These routes only make sense if the user is not logged in
Route::group(array('before' => 'guest'), function() {

	Route::get('signup', array('as' => 'signup', 'uses' => 'UserController@signUpForm'));

	Route::post('newuser', array('as' => 'newuser', 'uses' => 'UserController@createNewUser'));

	Route::get('login', array('as' => 'login', 'uses' => 'UserController@loginForm') );

	Route::get('password_forgotten', array('as' => 'password_forgotten', function() { return View::make('users.password_forgotten'); }));

	Route::post('reset_password', array('as' => 'reset_password', 'uses' => 'UserController@resetPassword' ));

	Route::get('reset_password_successs', array('as' => 'reset_password_success', function() { return View::make('users.reset_password_success'); }));

	Route::post('doLogin', array('as' => 'doLogin', 'uses' => 'UserController@loginUser') );

	Route::get('logout_success', array('as' => 'logout_success', function() {  return View::make('users.logout_success'); }) );


});


// These routes only make sense if the user is logged in

Route::group(array('before' => 'auth'), function() {

	Route::post('updateProfile', array('as' => 'updateProfile', 'uses' => 'UserController@updateProfile'));

	Route::get('profile', array('as' => 'profile', 'uses' => 'UserController@profilePage') );

	Route::post('doLogout', array('as' => 'doLogout', 'uses' => 'UserController@logoutUser') );

	Route::get('signup_success', array('as' => 'signup_success', function() {  return View::make('users.signup_success'); }) );

	Route::get('profile_update_success', array('as' => 'profile_update_success', function() {  return View::make('users.profile_update_success'); }) );

	Route::any('videos/upload/{step}', array('as' => 'video_upload', 'uses' => 'VideoController@uploadProcess'));


	Route::post('/ajax/dropbox', array('as' => 'video_dropbox', 'uses' => 'VideoController@videoUpload') );

});


Route::get('test', function() { return View::make('users.signup_success'); });

