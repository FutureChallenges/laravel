<?php

use Illuminate\Support\MessageBag;

class UserController extends Controller {


	public function signUpForm() {

		return View::make('users.signup');
	}

	public function loginForm() {

		return View::make('users.login');		
	}

	public function profilePage() {

		return View::make('users.profile');
	}

	public function loginUser() {

		$password = Input::get('password');

		if (Auth::attempt(array('username' => Input::get('username'), 'password' => $password), Input::get('remember'))) {
			// check if password needs rehash and recompute hash if this is true
			$user = Auth::user();

			if (Hash::needsRehash($user->password)) {
				$user->password = Hash::make($password);
				$user->save();
			}
			// was the user trying to get to a specific page without being logged in? If yes, redirect him
			// to the intended page now
			return Redirect::intended();
		}

		return Redirect::route('login')->withErrors(new MessageBag(array('login' => 'error')))->withInput(Input::except('password'));
	}

	public function logoutUser() {

		Auth::logout();

		return Redirect::route('logout_success');
	}

	public function createNewUser() {


		$rules = array(
			'username' => 'alpha_dash|required|unique:users,username|min:3',
			'email' => 'email',
			'password' => 'required|confirmed|min:6'
 		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
		    return Redirect::route('signup')
		    	->withErrors($validator)
			    ->withInput(Input::except(array('password', 'password_confirmation')));	
		}

		$new_user = new User();
		$new_user->username = Input::get('username');
		$new_user->password = Hash::make(Input::get('password'));
		$new_user->email = Input::get('email');
		$new_user->password_token = str_random(60);
		$new_user->save();

		Auth::login($new_user);

		return Redirect::route('signup_success');
	}

	public function updateProfile() {


		Validator::extend('is_correct_password',
			function($attribte, $value) {
				return Hash::check($value, Auth::user()->password);
			}
		);

		$rules = array(
			'old_password' => 'required|is_correct_password',
			'email' => 'email',
			'new_password' => 'confirmed|min:6'
 		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::route('profile')
		    	->withErrors($validator)
			    ->withInput(Input::except(array('old_password', 'new_password', 'new_password_confirmation')));	
		}


		$user = Auth::user();

		$new_password = Input::get('new_password');
		if ($new_password) {
			$user->password = Hash::make($new_password);
			$user->password_token = str_random(60);
			// the password has been changed and the time of the change recorded
			// users who logged in before this time will be automatically logged out
			// however, this user should not! therefore, we log him in again
			Auth::login($user, Auth::viaRemember());
		}

		$user->email = Input::get('email');

		$user->save();

		return Redirect::route('profile_update_success');
	}

	function resetPassword() {

		// Feature not yet implemented
		return Redirect::route('reset_password_success');
	}


}