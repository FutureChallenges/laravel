<?php

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Carbon\Carbon;

class VideoController extends Controller {
	

	
	public function uploadProcess($step) {

		list($submit, $cancel) = array(Input::get('submit'), Input::get('cancel'));

		if (($cancel || $submit) && !Request::isMethod('post')) {
			return Redirect::route('video_upload', array('step' => 1));				
		} 

		if ($cancel) {

			Video::destroy(Session::get('uploading_video'));

			Session::forget("uploading_video");
			Session::forget("uploading_step");

			return Redirect::route('video_upload', array('step' => 'canceled'));
		}

		$date_format = Video::TIME_FORMAT;

		if ($step == 1) {

			if ($submit) {

				$rules = array(
					'title' => 'required',
					'location' => 'required',
					'time' => "required|date_format:${date_format}",
					'category' => "in:" . implode(",", Video::$categories),
					'terms_and_conditions' => 'accepted'
				);

				$messages = array(
					'date_format' => 'The :attribute does not match the format ' . trans('videos.date_format')
				);

				$validator = Validator::make(Input::all(), $rules, $messages);

				if ($validator->fails()) {
				    return Redirect::route('video_upload', array('step' => 1))
				    	->withErrors($validator)
					    ->withInput(Input::all());	
				}

				$video = Video::find(Session::get('uploading_video'));

				if (!$video) {
					$video = new Video();
				}

				$video->fill(array_merge(
						Input::only('title', 'location', 'category', 'statement', 'description'),
						array(
							'uploader' => Auth::user()->id, 
							'time' => Carbon::createFromFormat($date_format, Input::get('time'))->hour(0)->minute(0)->second(0)
							)
					));

				$video->save();

				Session::set('uploading_video', $video->id);

				Session::set('uploading_step', 2);

				return Redirect::route('video_upload', array('step' => 2));
			} 

			// do nothing

		} else if ($step == 2) {

			if ($submit) {


				$response = $this->videoUpload($filename);

				$video = Video::find(Session::get('uploading_video'));
				if (!$video) {
					return Redirect::route('video_upload', array('step' => 1));
				}
				$video->original_file = $filename;
				$video->save();

				Session::set('uploading_step', 'success');

				if (Request::ajax() || Request::input('ajaxForm')) {
					return Response::make(ajaxform_json_encode($response));
				} else {
					return Redirect::route('video_upload', array('step' => 'success'));
				}

			} else if (Session::get('uploading_step') != 2) {

				return Redirect::route('video_upload', array('step' => 1));				
			}
		

		} else if ($step == 'success') {

			if (Session::get('uploading_step') != 'success') {

				return Redirect::route('video_upload', array('step' => 1));				

			} else {

				Session::forget("uploading_video");
				Session::forget("uploading_step");
			}

		} else if ($step != 'canceled') {

			return Redirect::route('video_upload', array('step' => 1));				

		}

		$trans_categories = array();

		foreach (Video::$categories as $cat) {
			$trans_categories[$cat] = trans("videos.categories." . $cat);
		}

		return View::make('videos.upload')->with(
			array(
				'step' => $step,
				'date_format' => $date_format,
				'categories' => $trans_categories
			)
		);

	}

	public function videoUpload(&$filename) {

		if (Input::hasFile('video_file')) {
			$file = Input::file('video_file');
			$filename = str_random(40);
			$file->move(storage_path() . "/videos", $filename);
			Log::info("file " . $file->getClientOriginalName() . " successfully uploaded");			

			return array(
					"statusCode" => SymfonyResponse::HTTP_OK,
					"msg" => Lang::get('videos.upload_successful', array("file" => $file->getClientOriginalName()))
				);

		} else {
			throw new HttpException(SymfonyResponse::HTTP_BAD_REQUEST, Lang::get('videos.max_size_exceeded'));
		}
	}
}

