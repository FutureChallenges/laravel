@extends('layouts.main')

@section('top')
	@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('users.error_warning') }}
		</div>
	@else
		<div class="alert alert-info">
			{{{ trans('general.welcome_prefix') }}}  <strong>{{{ Auth::user()->username }}}</strong>. {{{ trans('users.profile_instructions') }}}
		</div>
	@endif
@stop

@section('main')

{{ Form::model(Auth::user(), array('route' => 'updateProfile', 'role' => 'form', 'class' => 'form-horizontal')) }}

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}


<div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
	{{ Form::label('old_password', trans('users.old_password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('old_password', array('class' => 'form-control')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('old_password', '<label class="control-label" for="old_password">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.old_password_help') }}</span>
	</div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
	{{ Form::label('email', trans('users.email'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email_placeholder'))) }}
	</div>
	{{-- Traps for evil bots --}}
	{{ Form::hidden('email2') }}
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('email', '<label class="control-label" for="email">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.email_help') }}</span>
	</div>
</div>

<div class="row">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="alert alert-info">
			<strong>{{{ trans('users.password_change_instructions') }}}</strong>
		</div>
	</div>
</div>

<div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
	{{ Form::label('new_password', trans('users.new_password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('new_password', array('class' => 'form-control')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('new_password', '<label class="control-label" for="password">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.password_help') }}</span>
	</div>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
	{{ Form::label('new_password_confirmation', trans('users.new_password_confirmation'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('new_password_confirmation', array('class' => 'form-control')) }}
	</div>
</div>



{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}


<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
{{ Form::submit(trans('users.update_button'), array('class' => 'btn btn-primary')) }}
</div>
</div>
{{ Form::close() }}

@stop

@section('sidebar')
	<p>
		Some enlightening words from the bright minds behind irrepressible voices
	</p>
@stop
