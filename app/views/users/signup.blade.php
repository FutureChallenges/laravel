@extends('layouts.main')

@section('top')
	@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('users.error_warning') }}
		</div>
	@else
		<div class="alert alert-info">
		  	<strong>{{ trans('general.welcome') }}</strong>
			{{ trans('users.signup_instructions') }}
		</div>
	@endif
@stop

@section('main')

{{ Form::open(array('route' => 'newuser', 'role' => 'form', 'class' => 'form-horizontal')) }}

<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
	{{ Form::label('username', trans('users.username'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => trans('users.username_placeholder'))) }}
	</div>
	{{-- Traps for evil bots --}}
	{{ Form::hidden('name') }}
	{{ Form::hidden('address') }}
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('username', '<label class="control-label" for="username">:message</label>') }}
	</div>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
	{{ Form::label('password', trans('users.password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('password', array('class' => 'form-control')) }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('password', '<label class="control-label" for="password">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.password_help') }}</span>
	</div>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
	{{ Form::label('password_confirmation', trans('users.password_confirmation'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
	{{ Form::label('email', trans('users.email'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email_placeholder'))) }}
	</div>
	{{-- Traps for evil bots --}}
	{{ Form::hidden('email2') }}
	<div class="col-sm-offset-2 col-sm-10">
		{{ $errors->first('email', '<label class="control-label" for="email">:message</label>') }}
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.email_help') }}</span>
	</div>
</div>

{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}


<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
{{ Form::submit(trans('users.signup_button'), array('class' => 'btn btn-primary')) }}
</div>
</div>
{{ Form::close() }}

@stop

