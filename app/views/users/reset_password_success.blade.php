@extends('layouts.onecolumn')


@section('top')
@stop
@section('main')

	<div class="jumbotron alert alert-success">
		<h2>
			<strong>MOCK-UP</strong> {{{ trans('users.password_was_reset') }}} <strong>MOCK-UP</strong>
		</h2>
		<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('login')}}">{{{ trans('general.login_page') }}}</a>.</p>
	</div>

@stop