@extends('layouts.onecolumn')

@section('top')
@stop

@section('main')

	<div class="jumbotron alert alert-success">
		<h2>
			{{{ trans('users.profile_update_success') }}}
		</h2>
		<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('home')}}">{{{ trans('general.home_page') }}}</a>.</p>
	</div>

@stop