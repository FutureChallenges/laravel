@extends('layouts.main')

<script>
	function reset_password() {
		var dialog = $('#password_reset');
		dialog.one('hidden.bs.modal', function(e) {
			$('#reset_done').modal('show');
		});
		dialog.modal('hide');
	}
</script>
<div class="modal fade" data-backdrop="static" id="password_reset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">{{{ trans('users.password_forgotten_question') }}}</h4>
      </div>
      <div class="modal-body">
      	<strong>MOCK-UP</strong> {{{ trans('users.password_reset_msg') }}} <strong>MOCK-UP</strong>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('general.close') }}}</button>
        <button type="button" class="btn btn-danger" onclick="reset_password();">{{{ trans('users.reset_password') }}}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" data-backdrop="static" id="reset_done" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><strong>MOCK-UP</strong> {{{ trans('users.password_was_reset') }}} <strong>MOCK-UP</strong></h4>
      </div>
      <div class="modal-body text-right">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('general.close') }}}</button>
      </div>
    </div>
  </div>
</div>


@section('top')
	@if ($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ trans('general.warning') }}</strong>
			{{ trans('users.login_failed') }}
		</div>
	@elseif (Session::has('url.intended'))
		<div class="alert alert-warning">
			{{ trans('users.login_instructions_redirect') }}
		</div>
	@else
		<div class="alert alert-info">
		  	<strong>{{ trans('general.welcome') }}</strong>
			{{ trans('users.login_instructions') }}
		</div>
	@endif
@stop

@section('main')

{{ Form::open(array('route' => 'doLogin', 'role' => 'form', 'class' => 'form-horizontal')) }}

<div class="form-group {{ $errors->any() ? 'has-error' : ''}}">
	{{ Form::label('username', trans('users.username'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::text('username', null, array('class' => 'form-control' )) }}
	</div>
</div>

{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}

<div class="form-group {{ $errors->any() ? 'has-error' : ''}}">
	{{ Form::label('password', trans('users.password'), array('class' => 'text-right col-sm-2')) }}
	<div class="col-sm-10">
		{{ Form::password('password', array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
	{{ Form::submit(trans('users.login_button'), array('class' => 'btn btn-primary')) }}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<div class="checkbox ">
			<label>
				{{ Form::checkbox('remember') }} {{{ trans('users.remember_me') }}}
			</label>
		</div>
	</div>
	<div class="col-sm-offset-2 col-sm-10">
		<span class="help-block">{{ trans('users.remember_me_explained') }}</span>
	</div>
</div>


{{-- Traps for evil bots --}}
{{ Form::hidden('telephone') }}

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-4">
		<a id="reset_link" type="button" href="{{ URL::route('password_forgotten') }}" class="btn btn-warning" data-toggle="modal" data-target="#password_reset">{{{trans('users.password_forgotten')}}}</a>
	</div>
</div>

{{ Form::close() }}

 <script>
    $(window).load(function() {
      $('#reset_link').removeAttr('href');
    });
</script>
@stop