@extends('layouts.onecolumn')

@section('top')
@stop
@section('main')

	<div class="jumbotron alert alert-warning">
    	<h2>{{{ trans('users.password_forgotten_question') }}}</h2>
      	<p><strong>MOCK-UP</strong> {{{ trans('users.password_reset_msg') }}} <strong>MOCK-UP</strong></p>
      	<div class="vspace-above-15">
      		{{ Form::open(array('route' => 'reset_password', 'role' => 'form', 'id' => 'optoutForm')) }}
		        <a type="button" class="btn btn-default" href="{{ URL::route('login') }}">{{{ trans('general.nothanks') }}}</a>
		        <button type="submit" class="btn btn-primary">{{{ trans('users.reset_password') }}}</button>
	        {{ Form::close() }}
	    </div>
	</div>

@stop