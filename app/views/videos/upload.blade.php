@extends('layouts.onecolumn')


@section('header')

@parent
<div class="page-header container-fluid">
  <h2>{{ trans('videos.video_upload') }} <small>{{
   ($step == 'success' ? trans('videos.completed') : 
   ($step == 'canceled' ? trans('videos.canceled') :
	   trans('videos.video_upload_step', array('step'=> $step, 'total' => 2)))) }}</small></h2>
</div>


@stop

@section('top')

	@if ($errors->any())
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		  	<strong>{{ trans('general.warning') }}</strong>
			{{ trans('videos.error_warning') }}
		</div>
	@elseif ($step == 1)
		<div class="alert alert-info">
				{{ trans('videos.upload_banner') }}
		</div>
	@elseif ($step == 2 )


	@endif
@stop


@section('main')

@if ($step == 1)

{{ Form::model(Video::find(Session::get('uploading_video')), array('url' => URL::route('video_upload', array('step' => $step, 'submit' => true)),  'enctype' => 'application/x-www-form-urlencoded', 'role' => 'form')) }}
{{-- Traps for evil bots --}}
{{ Form::hidden('name') }}
{{ Form::hidden('address') }}
{{ Form::hidden('email2') }}
{{ Form::hidden('telephone') }}

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('videos.basic_info') }}</h4>
			<p>{{ trans('videos.basic_info_desc') }}</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
			{{ Form::label('title', trans('videos.title')) }}
			{{ Form::text('title', null, array('class' => 'form-control' )) }}
			{{ $errors->first('title', '<label class="control-label" for="title">:message</label>') }}
		</div>		
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
			{{ Form::label('location', trans('videos.location')) }}
			{{ Form::text('location', null, array('class' => 'form-control' )) }}
			{{ $errors->first('location', '<label class="control-label" for="location">:message</label>') }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group {{ $errors->has('time') ? 'has-error' : ''}}">
			{{ Form::label('time', trans('videos.time')) }}
			{{ Form::text('time', null, array('class' => 'form-control', 'placeholder' => trans('videos.date_format') )) }}
			{{ $errors->first('time', '<label class="control-label" for="time">:message</label>') }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group {{ $errors->has('terms_and_conditions') ? 'has-error' : ''}}">
			<div class="checkbox">
			    <label>
			      {{ Form::checkbox('terms_and_conditions') }} {{ trans('videos.terms_and_conditions') }}
			    </label>
			</div>
			{{ $errors->first('terms_and_conditions', '<label class="control-label" for="terms_and_conditions">:message</label>') }}
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('videos.additional_info') }}</h4>
			<p>{{ trans('videos.additional_info_desc') }}</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('description', trans('videos.description')) }}
			{{ Form::textarea('description', null, array('class' => 'form-control' )) }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('statement', trans('videos.statement')) }}
			{{ Form::textarea('statement', null, array('class' => 'form-control' )) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('tags', trans('videos.tags')) }}
			{{ Form::text('tags', null, array('class' => 'form-control' )) }}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('category', trans('videos.category')) }}
			{{ Form::select('category', $categories, null, array('class' => 'form-control' )) }}
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			{{ Form::submit( trans('videos.to_video_upload'), array('class' => 'btn btn-primary')) }}
		</div>
	</div>
</div>

{{ Form::close() }}

{{ Form::open(array('url' => URL::route('video_upload', array('step' => $step, 'cancel' => true)), 'role' => 'form')) }}

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			{{ Form::submit( trans('videos.video_cancel'), array('class' => 'btn btn-warning')) }}
		</div>
	</div>
</div>

{{ Form::close() }}





@elseif ($step == 2)

<script>

	var uploadXhr = null;
	var uploadAjaxConfig = null;
	var uploadMaxSize = {{ return_bytes(ini_get('upload_max_filesize')) }};

	var uploadMsg = {
		aborted: '{{{ trans('videos.upload_aborted') }}}',
		failed: '{{{ trans('videos.upload_failed') }}}',
		uploading: '{{{ trans('videos.uploading') }}}',
		upload_prompt: '{{{ trans('videos.upload_prompt') }}}',
		wait: '{{{ trans('videos.wait') }}}',
		max_size_exceeded: '{{{ trans('videos.max_size_exceeded') }}}',
		upload_completed: '{{{ trans('videos.upload_completed') }}}'
	}	

	function handleVideoFiles(files) {
		if (!files || files.length === 0) {
			return;
		}
		var file = files[0];
	
		if (!FormData) {
			return;
		}

		if (file.size && file.size > uploadMaxSize) {
			showError(uploadMsg.max_size_exceeded);
			return;
		}

		var formData = new FormData();
		formData.append("_token", '{{ Session::getToken() }}');
		formData.append("video_file", file);

		var fileName = file.name;

		uploadXhr = $.ajax({
			url: "{{ URL::route('video_dropbox') }}",
			contentType: false,
			type: "POST",
			processData: false,
			data: formData,
			beforeSend: function(xhr) {
				prepareUpload(fileName);
			},
			complete: function() {
    			uploadAjaxConfig = this;
    		},
    		success: function(response) {
				var json = parseAjaxFormJSON(response);
    			showSuccess(json.msg);
    		},
    		error: function(xhr, status, error) {
    			var json = parseAjaxFormJSON(xhr.responseText);
				handleError(xhr.status, xhr.statusText, json.errorMsg, status === "abort");
    		},
    		xhr: function() {
                var xhr = $.ajaxSettings.xhr();
                if (xhr.upload) {
                    xhr.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position; /*event.position is deprecated*/
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        updateUploadBar(percent);
                    }, false);
                }
                return xhr;
            }
		});

	}

	function fileInputChanged() {

		var fileName, fileSize;
		if (this.files && this.files.length > 0) {
			fileName = this.files[0].name;
			fileSize = this.files[0].size;
		} else {
			fileName = this.value.split('/').pop().split('\\').pop();			
		}

		if (!fileName) {
			return;
		}
		if (fileSize && fileSize > uploadMaxSize) {
			showError(uploadMsg.max_size_exceeded);
			return;
		}

		$('#videoUploadForm').ajaxSubmit({
			uploadProgress: function(event, pos, total, perc) {
				updateUploadBar(perc);
    		},
    		data: {
    			"ajaxForm": 1
    		},
    		beforeSend: function(xhr) {    			
				uploadXhr = xhr;    			
				prepareUpload(fileName);
			},
			//iframe: true,
			error: function(xhr, status, error) {
				var json = parseAjaxFormJSON(xhr.responseText);
				handleError(xhr.status, xhr.statusText, json.errorMsg, status === "aborted" || status === "abort");
			},
    		complete: function() {
    		},
    		success: function(response) {

    			var json = parseAjaxFormJSON(response);

	    		if (json.statusCode === 200) {
	    			showSuccess(json.msg);	    			
	    		} else {
	    			handleError(json.statusCode, json.statusText, json.errorMsg, false);
	    		}

    		}
    	});
	}

	function parseAjaxFormJSON(str) {

		var json = null;
		try {
			json = $.parseJSON(str);
		} catch(e) {
			str = $(str).html();	    			
		}

		if (!json) {
			try {
				json = $.parseJSON(str);
			} catch(e) {
				return {};
			}
		}

		if (!json) {
			json = {};
		}
		return json;
	}


	function handleError(statusCode, statusText, errorMsg, aborted, retry) {

		if (retry == undefined) {
			retry = !aborted;
		}

		var msg = (aborted ? uploadMsg.aborted : uploadMsg.failed + 
			(errorMsg ? ': ' + errorMsg : ''));

		showError(msg, retry);
	}

	function cancelUpload(e) {
		if (uploadXhr) {
			uploadXhr.abort();
		}		
	}

	function updateUploadBar(perc, msg) {
		var percString = perc + "%";
		$('#videoDrop .progress-bar').attr('aria-valuenow', perc);
		$('#videoDrop .progress-bar').width(percString);
		$('#videoDrop .progress-bar').html(msg ? msg : percString);	
	}

	function prepareUpload(fileName) {


		$('#videoDrop').unbind('click', selectFile);
		$('#videoDrop').unbind('drop', fileDropped);

		$('#videoDrop').attr('class', 'upload upload-active');

		$('#cancel-upload').show();

		$('#videoDrop .progress-area').show();
		$('#videoDrop .upload-prompt').hide();
		$('#videoDrop .message-area').hide();

		$('#videoDrop .progress-area h4').html(uploadMsg.uploading + " " + fileName);		
		$('#videoDrop .progress-bar').width("100%");
		$('#videoDrop .progress-bar').html(uploadMsg.wait);
		$('#videoDrop .progress').addClass("progress-striped active");
		$('#videoDrop .progress-bar').attr('class', 'progress-bar');
	}

	function showError(msg, retry) {

		$('#videoDrop').unbind('click', selectFile);
		$('#videoDrop').unbind('drop', fileDropped);

		$('#videoDrop').attr('class', 'upload alert-danger');

		$('#videoDrop .progress-area').hide();
		$('#videoDrop .upload-prompt').hide();
		$('#videoDrop .message-area').show();

		if (retry) {
			$('#retry-button').show();
		} else {
			$('#retry-button').hide();			
		}
		$('#videoDrop .message-area h4').html(msg);
	}

	function showSuccess(msg) {

		updateUploadBar(100, uploadMsg.upload_completed);

		$('#videoDrop').unbind('click', selectFile);
		$('#videoDrop').unbind('drop', fileDropped);

		$('#videoDrop').attr('class', 'upload upload-active');

		$('#cancel-upload').hide();
		$('#videoDrop .progress').removeClass('progress-striped active');
		$('#videoDrop .progress-area h4').html(msg);
		$('#videoDrop .progress-bar').attr('class', 'progress-bar progress-bar-success');

		window.location.replace('{{ URL::route('video_upload', array('step' => 'success')) }}');
	}

	function reinitUploadArea() {

		uploadXhr = uploadAjaxConfig = null;

		$('#videoDrop .upload-prompt').show();
		$('#videoDrop .message-area').hide();
		$('#videoDrop .progress-area').hide();
		$('#videoDrop').attr('class', 'upload upload-select');
		$('#videoDrop .upload-prompt h4').html(uploadMsg.upload_prompt);
		$('#videoDrop').bind('click', selectFile);
		$('#videoDrop').bind('drop', fileDropped);

	}

	function selectFile() {

    	$('#fileInput').click();	
	}

	function fileDropped(e) {
		e.stopPropagation(); e.preventDefault(); 

		var dt = e.originalEvent.dataTransfer;
		if (dt && $.inArray("Files", dt.types) > -1) {
			handleVideoFiles(dt.files);
		}
	}

	function initUploadArea() {


		$('#nojs-file-upload').hide();
		$('#video_submit').hide();
    	$('#videoDropGroup').show();

    	$('#fileInput').change(fileInputChanged);
    	$('#videoDrop').bind('dragenter', function(e) { 
    		e.stopPropagation(); e.preventDefault(); 
    	});
    	$('#videoDrop').bind('dragleave', function(e) { 
    		e.stopPropagation(); e.preventDefault(); 
    	});
    	$('#videoDrop').bind('dragover', function(e) { 
    		e.stopPropagation(); e.preventDefault(); 
    		var dt = e.originalEvent.dataTransfer;
    		if (dt) {
    			if ($.inArray("Files", dt.types) > -1) {
	    			dt.dropEffect = "copy";
	    		} else {
	    			dt.dropEffect = "move";
	    		}
    		} 
    	});
    	$('#cancel-upload').bind('click', cancelUpload);
	    $('videoUploadForm').ajaxForm();

	    $('#ok-button').bind('click', function(e) {
	    	reinitUploadArea();
	    	e.stopPropagation();
	    });

	    $('#retry-button').bind('click', function(e) {
	    	if (uploadAjaxConfig) {
    			$.ajax(uploadAjaxConfig);
	    	} else {
	    		$('#fileInput').trigger('change');
			}
	    	e.stopPropagation();
	    });

	    reinitUploadArea();
	}

    $(window).load(function() {
    	initUploadArea();
	});


</script>

{{ Form::open(array('url' => URL::route('video_upload', array('step' => $step, 'submit' => true)), 'enctype' => 'multipart/form-data', 'role' => 'form', 'id' => 'videoUploadForm')) }}

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<h4>{{ trans('videos.do_upload') }}</h4>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group" id="nojs-file-upload">
				{{ Form::label('title', trans('videos.upload_prompt_simple')) }}
				{{ Form::file('video_file', array('id' => 'fileInput', 'class' => 'btn btn-default')) }}
		</div>
	</div>
</div>
{{-- Form::file('video_file', array('id' => 'fileInput', 'style' => 'display:none;')) --}}
<div class="row" id="videoDropGroup" style="display:none">
	<div class="col-sm-6">
		<div class="form-group">
			<div id="videoDrop" class="upload upload-select" >
				<div class="progress-area row">
					<div class="col-xs-11">
						<div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
						   	0%
						  </div>
						</div>
					</div>
					<div class="col-xs-1">
						<button type="button" id="cancel-upload" class="close" aria-hidden="true">&times;</button>
					</div>
				   <h4 class="text-center"></h4>
				</div>
				<div class="message-area">
					<h4></h4>
					<div class="btn-group">
						<input id="ok-button" type="button" class="btn btn-sm btn-default" value="{{{ trans('videos.ok') }}}"></input>
						<input id="retry-button" type="button" class="btn btn-sm btn-default" value="{{{ trans('videos.retry') }}}"></input>
					</div>
				</div>
				<div class="upload-prompt">
					<img id="upload_icon" class="center-block" src="/resources/img/upload.png"></img>
					<h4 class="text-center"></h4>	
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::submit( trans('videos.video_submit'), array('id' => 'video_submit', 'class' => 'btn btn-primary')) }}
		</div>
	</div>
</div>

{{ Form::close() }}

{{ Form::open(array('url' => URL::route('video_upload', array('step' => $step, 'cancel' => true)), 'role' => 'form')) }}

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::submit( trans('videos.video_cancel'), array('class' => 'btn btn-warning')) }}
		</div>
	</div>
</div>

{{ Form::close() }}






@elseif ($step == 'success')

<div class="jumbotron alert alert-success">
	<h2>
		{{{ trans('videos.success') }}}
	</h2>
	<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('home')}}">{{{ trans('general.home_page') }}}</a>.</p>
</div>


@elseif ($step == 'canceled')

<div class="jumbotron alert alert-warning">
	<h2>
		{{{ trans('videos.canceled') }}}
	</h2>
	<p>{{{ trans('general.next_page_suggestion') }}} <a href="{{URL::route('home')}}">{{{ trans('general.home_page') }}}</a>.</p>
</div>


@endif

@stop

