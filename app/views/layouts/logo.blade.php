 <div class="container-fluid ">
 	<div class="row">
      <div class="col-sm-7">
	      <img src="/resources/img/logo.png" class="img-responsive vspace-above-15">
      </div>
      <div class="col-sm-5 vspace-above-15">
      	@if (Session::get('tor.warning'))
	      @if (!Session::get('tor.error'))
		      <div class="alert alert-danger">
		      	<h4><strong>{{{ trans('warning.anonymity_header') }}}</strong></h4>
		      	<p>{{{ trans('warning.anonymity_msg') }}}
		      	<a class="alert-link">{{{ trans('general.learn_more') }}}</a></p>
		      </div>
	      @else
			 <div class="alert alert-warning">
		      	<h4><strong>{{{ trans('warning.anonymity_unclear_header') }}}</strong></h4>
		      	<p>{{{ trans('warning.anonymity_unclear_msg') }}}
		      	<a class="alert-link">{{{ trans('general.learn_more') }}}</a></p>
		      </div>
	      @endif
	    @else
	      <div class="alert alert-success">
	      	<h4><strong>{{{ trans('warning.anonymity_ok_header') }}}</strong></h4>
	      	<p>{{{ trans('warning.anonymity_ok_msg') }}}
	      </div>
	    @endif
	  </div>
  	</div>
 </div>