<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>{{{ trans('general.title') }}}</title>

    {{-- Bootstrap --}}
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/utils.css" rel="stylesheet">

    {{-- <h2>INTENDED: {{ Session::get('url.intended') }} </h2> --}}

    {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
    {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
        {{-- jQuery (necessary for Bootstrap's JavaScript plugins) --}}
    <script src="/resources/js/jquery.min.js"></script>
    {{-- Include all compiled plugins (below), or include individual files as needed --}}
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/js/jquery.form.js"></script>

<div class="page-wrap">

    @section('torwarning_dialog')
    @if (Session::get('tor.warning') && !Session::get('tor.optout'))
      <div class="modal fade" id="tor_warning" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
           <div class="modal-header {{ (Session::get('tor.error') ? 'alert-warning' : 'alert-danger') }}">
            @if (!Session::get('tor.error'))
                <h2>{{{ trans('warning.notor_header') }}} </h2>
                <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
            @else
                <h2>{{{ trans('warning.notor_unsure_header')}}} </h2>
                <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
            @endif
           </div>
           <div class="modal-body {{ (Session::get('tor.error') ? 'alert-warning' : 'alert-danger') }} text-right">
              <button type="button" onclick="$('#optoutForm').submit();" class="btn btn-default" data-dismiss="modal"> <?php echo e(trans('warning.browse_without_tor')); ?></button>
              <a type="button" href="https://www.torproject.org/download/" class="btn btn-primary">{{{ trans('warning.download_tor') }}} </a>
           </div>
          </div>
        </div>
      </div>
      {{ Form::open(array('route' => 'doOptout', 'role' => 'form', 'id' => 'optoutForm')) }}
      {{ Form::close() }}
      <script>
        $(window).load(function() {
          $('#tor_warning').modal('show');
        });
      </script>
    @endif
    @show


    @yield('header')

    @if (isset($js_required)) 
      <noscript>
        <div class="container">
          <div class="jumbotron alert-warning">
            <h2>{{{ trans('warning.js_disabled') }}}</h2>
            <h4>{{{ trans('warning.js_message') }}}</h4>        
          </div>
        </div>
      </noscript>
    @endif
      
      @section('torwarning_noscript')
      @if (Session::get('tor.warning') && !Session::get('tor.optout'))
      <noscript>
        <div class="container">
          <div class="jumbotron {{ (Session::get('tor.error') ? 'alert-warning' : 'alert-danger') }}">
              @if (!Session::get('tor.error'))
                  <h2>{{{ trans('warning.notor_header') }}} </h2>
                  <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
              @else
                  <h2>{{{ trans('warning.notor_unsure_header')}}} </h2>
                  <h4 class="text-warning">{{{ trans('warning.notor_msg') }}}</h4>
              @endif
              {{ Form::open(array('route' => 'doOptout', 'class' => 'vspace-above-15', 'role' => 'form', 'id' => 'optoutForm')) }}
                <button type="submit" class="btn btn-default"> <?php echo e(trans('warning.browse_without_tor')); ?></button>
                <a type="button" href="https://www.torproject.org/download/" class="btn btn-primary">{{{ trans('warning.download_tor') }}} </a>
              {{ Form::close() }}
          </div>
        </div>
      </noscript>
     @endif
     @show

    @yield('content')

</div>


<footer class="site-footer">
  <div class="well well-sm" style="margin-bottom: 0px;">
  {{{ trans('general.copyright_note') }}}
  </div>
</footer>

  </body>
</html>
