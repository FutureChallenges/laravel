<nav class="navbar navbar-default vspace-above-15" role="navigation">

  @if (Auth::check())
  @endif

    <div class="navbar-header navbar-right">

    	@if (Auth::check())
         {{ Form::open(array('route' => 'doLogout', 'role' => 'search', 'id' => 'logoutForm')) }}
          <a class="navbar-link hspace-left-15 hspace-right-5" href="{{ URL::to('profile') }}">{{ trans("navbar.logged_in_as") }}<strong>{{{Auth::user()->username}}}</strong></a>
          <a class="navbar-btn btn btn-primary" href="{{{ URL::to('profile') }}}"><span class="glyphicon glyphicon-user"></span> {{{ trans("navbar.profile")}}}</a>
          <button type="submit" class="btn navbar-btn btn-default hspace-right-5">{{{ trans("navbar.logout")}}}</button>
      @else
         <a class="navbar-btn btn btn-primary hspace-left-5" href="{{{ URL::to('login') }}}">{{{ trans("navbar.login")}}}</a>
      	 <a class="navbar-btn btn btn-link" href="{{ URL::to('signup') }}">{{{ trans("navbar.signup")}}}</a>
     	@endif
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      @if (Auth::check())
       {{ Form::close() }}
      @endif
    </div>
    
 	<div class="navbar-collapse collapse" id="navbar-collapse-1">
{{-- replace the above div element with this one to uncollapse the navbar by default:  <div class="navbar-collapse collapse in" id="navbar-collapse-1"> --}}

      <ul class="nav navbar-nav">
          <li><a href="{{ url('/') }}">{{{ trans("navbar.home") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.videos") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.community") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.engage") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.news") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.about") }}}</a></li>
          <li><a href="{{ url('/') }}">{{{ trans("navbar.contact") }}}</a></li>	        
      </ul>
  </div>
</nav>

