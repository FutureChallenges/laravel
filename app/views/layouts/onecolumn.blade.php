@extends('layouts.master')

@section('header')

  @include('layouts.logo')
  @include('layouts.navbar')    

@stop

@section('content')

<div class="container-fluid">

  <div class="row">

    <div class="col-sm-12">

      @section('top')
        <h2> This is a header where you can place some important messages </h2>
      @show


    </div>

  </div>

  <div class="row">

    <div class="col-sm-12">

      @section('main')
        <h3> This is where the main content of the page will go. </h3>
        <h3>
        @if (Auth::check()) 
         {{ Form::open(array('route' => 'doLogout', 'role' => 'form', 'id' => 'logoutForm')) }}
          For now, you can either <a href="{{ URL::route('profile') }}">edit your profile</a> or <button class="btn btn-primary" type="submit">log out</button>
          {{ Form::close() }}
        @else
          For now, you can either <a href="{{ URL::route('signup') }}">sign up</a> or <a href="{{ URL::route('login')}}">log in</a> if you already have an account.
        @endif
      </h3>
      @show

    </div>
   
  </div>

</div>

@stop


