@extends('layouts.master')

{{-- Layoout with logo, navigation bar and a 3-areas content: top, main and sidebar --}}

@section('header')

  @include('layouts.logo')
  @include('layouts.navbar')    

@stop

@section('content')

<div class="container-fluid">

  <div class="row">

    <div class="col-sm-12">

      @section('top')
        <h2> Welcome to the test home page! </h2>
      @show


    </div>

  </div>

  <div class="row">

    <div class="col-sm-8">

      @section('main')
        <h3>
        @if (Auth::check()) 
         {{ Form::open(array('route' => 'doLogout', 'role' => 'form', 'id' => 'logoutForm')) }}
          To upload a video, click <a href="{{ URL::route('video_upload', array('step' => 1)) }}">here!</a>
          {{ Form::close() }}
        @else
          In order to upload a video, <a href="{{ URL::route('signup') }}">sign up</a> or <a href="{{ URL::route('login')}}">log in</a> if you already have an account.
        @endif
      </h3>
      @show

    </div>
    <div class="col-sm-4">

      @section('sidebar')
        <h4>
        </h4>
      @show

    </div>

  </div>

</div>

@stop


